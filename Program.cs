﻿using Recorder.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Recorder.Core.Models;
using System.Management;
using Recorder.Core;

namespace Recorder.Tester
{
    class Program
    {
        static void Main(string[] args)
        {
            Record();
        }

        private static void Record()
        {
            var vd = new UsbDiscovery();

            var videoDevices = new AForge.Net.VideoDiscovery().GetDevices().ToList();
            var audioDevices = new CSCore.AudioDiscovery().GetDevices().ToList();

            PrintDevices("Video", videoDevices);
            var videoDeviceIndex = GetIndex("Video", videoDevices);

            var videoDevice = (VideoDevice)videoDevices[videoDeviceIndex];
            var filePathService = new DefaultSavePathService(@"C:\Users\Lars\Desktop\");
            var videoRecorder = new AForge.Net.VideoRecorder(videoDevice, filePathService);

            PrintDevices("Audio", audioDevices);
            var audioDeviceIndex = GetIndex("Audio", audioDevices);
            var audioDevice = (AudioDevice)audioDevices[audioDeviceIndex];
            var audioFilePath = @"C:\Users\Lars\Desktop\test.wav";
            var audioRecorder = new CSCore.AudioRecorder(audioDevice, filePathService);

            var audioDevice2Index = GetIndex("Audio 2", audioDevices);
            var audioDevice2 = (AudioDevice)audioDevices[audioDevice2Index];
            var audioFilePath2 = @"C:\Users\Lars\Desktop\test-2.wav";
            var audioRecorder2 = new CSCore.AudioRecorder(audioDevice2, filePathService);

            videoRecorder.Start();
            audioRecorder.Start();
            audioRecorder2.Start();

            Console.WriteLine("\n\nYoooo! Press any key to exit.");
            Console.ReadKey();

            videoRecorder.Stop();
            audioRecorder.Stop();
            audioRecorder2.Stop();
        }

        private static void PrintDevices(string deviceType, IEnumerable<IDevice> devices)
        {
            Console.WriteLine(string.Format("\n{0}{1} Devices", char.ToUpper(deviceType[0]), deviceType.Substring(1).ToLower()));
            Console.WriteLine("---------------------------------------");
            for (var i = 0; i < devices.Count(); i++)
            {
                var device = devices.ElementAt(i);
                Console.WriteLine(string.Format("{0}) {1}", (i + 1), device.Name));
            }
        }
        private static int GetIndex(string deviceType, IEnumerable<IDevice> devices)
        {
            Console.Write(string.Format("\nPick the input {0} device: ", deviceType.ToLower()));

            var input = Console.ReadLine();

            var inputIndex = -1;
            if (!int.TryParse(input, out inputIndex))
            {
                Console.WriteLine("Invalid device index. Please try again!");
                return GetIndex(deviceType, devices);
            }
            var deviceIndex = inputIndex - 1;
            if(deviceIndex < 0 || inputIndex > devices.Count())
            {
                Console.WriteLine("Invalid device index. Please try again!");
                return GetIndex(deviceType, devices);
            }

            return deviceIndex;
        }
    }

    class UsbDiscovery : IVideoDiscovery
    {
        public IEnumerable<IVideoDevice> GetDevices()
        {
            List<VideoDevice> devices = new List<VideoDevice>();

            ManagementObjectCollection collection;
            using (var searcher = new ManagementObjectSearcher(@"Select * From Win32_USBHub"))
                collection = searcher.Get();

            foreach (var device in collection)
            {
                devices.Add(new VideoDevice {
                    DeviceId = (string)device.GetPropertyValue("DeviceID"),
                    PNPDeviceId = (string)device.GetPropertyValue("PNPDeviceID"),
                    Name =(string)device.GetPropertyValue("Description")
                });
            }

            collection.Dispose();
            return devices;
        }
    }

    class AudioDiscovery : IAudioDiscovery
    {
        public IEnumerable<IAudioDevice> GetDevices()
        {
            var devices = new List<AudioDevice>();
            ManagementObjectSearcher mo = new ManagementObjectSearcher("select * from Win32_SoundDevice");

            foreach (ManagementObject soundDevice in mo.Get())
            {
                devices.Add(new AudioDevice
                {
                    Name = soundDevice.GetPropertyValue("Name").ToString(),
                    DeviceId = soundDevice.GetPropertyValue("DeviceId").ToString(),
                    PNPDeviceId = soundDevice.GetPropertyValue("PNPDeviceID").ToString()
                });
            }

            return devices;
        }
    }
}
